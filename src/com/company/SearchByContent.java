package com.company;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;

import static java.nio.file.FileVisitResult.CONTINUE;

public class SearchByContent extends SimpleFileVisitor<Path> {

    private final String pattern;
    private int numMatches = 0;

    public SearchByContent(String pattern) {
        this.pattern = pattern;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if (!attrs.isRegularFile()) return CONTINUE;
        List<String> lines = Files.readAllLines(file, StandardCharsets.ISO_8859_1);
        for (String s : lines) {
            if (s.contains(pattern)) {
                numMatches++;
                System.out.println(file.toAbsolutePath());
                break;
            }
        }
        return CONTINUE;
    }

    void done() {
        System.out.println("Найдено: " + numMatches);
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) {
//        System.err.println(exc);
        return CONTINUE;
    }
}

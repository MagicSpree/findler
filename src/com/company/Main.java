package com.company;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Main {

    public static void main(String[] args){
        Path startingDir = Paths.get("/tmp");

        searchByName(startingDir, "index");

        String pattern = "This is the file we need";
        searchByContent(startingDir, pattern);
    }

    private static void searchByContent(Path startingDir, String pattern) {
        SearchByContent finder = new SearchByContent(pattern);
        try {
            Files.walkFileTree(startingDir, finder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        finder.done();
    }

    private static void searchByName(Path startingDir, String nameFile) {
        SearchByName searchByName = new SearchByName(nameFile);
        try {
            Files.walkFileTree(startingDir, searchByName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        searchByName.done();
    }
}
